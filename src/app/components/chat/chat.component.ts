import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { Subscription, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  texto: string = '';
  messageSubscription: Subscription = new Subscription;
  elementoSubscription: Subscription = new Subscription;
  elemento: HTMLElement;

  mensajes: any[] = [];

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit() {
    this.elemento = document.querySelector('#chat-mensajes');

    this.messageSubscription = this.chatService.getMessage()
    .subscribe(msg => {
      console.log(msg);
      this.mensajes = [...this.mensajes, msg];

      this.elementoSubscription = of(null)
      .pipe(delay(50))
      .subscribe(() => this.elemento.scrollTop = this.elemento.scrollHeight);
    });

  }

  ngOnDestroy() {
    this.messageSubscription.unsubscribe();
    this.elementoSubscription.unsubscribe();
  }

  enviar() {
    if(this.texto.trim().length === 0) {
      return;
    }
    this.chatService.sendMessage(this.texto);
    this.texto = '';
  }
}
